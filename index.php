<?php
    require_once 'ape.php';
    require_once 'frog.php';
    require_once 'animal.php';

    $sheep = new Animal("shaun");
    echo "<b>" . $sheep->name . "</b> <br>";
    echo $sheep->legs . "<br>";
    echo var_dump($sheep->cold_blooded) . "<br><br>";

    $sungokong = new Ape("kera sakti");
    echo "<b>" . $sungokong->name . "</b> <br>";
    echo $sungokong->legs . "<br>";
    $sungokong->yell(); 
    echo "<br><br>";

    $kodok = new Frog("buduk");
    echo "<b>" . $kodok->name . "</b> <br>";
    echo $kodok->legs . "<br>";
    $kodok->jump();

?>
